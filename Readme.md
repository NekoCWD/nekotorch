# Simple torch extension
![Quick Settings in gnome-mobile. Flashlight turned off](/screenshots/QuickSettings1.jpg){width=20%}
![Quick Settings in gnome-mobile. Flashlight turned on](/screenshots/QuickSettings2.jpg){width=20%}
![Extension Settings 1](/screenshots/ExtensionSettings1.jpg){width=20%}
![Extension Settings 2](/screenshots/ExtensionSettings2.jpg){width=20%}

## Installation guide
1. Open ``/etc/udev/rules.d/99-flash.rules`` in your faivourite edior
2. Paste this
    >``SUBSYSTEM=="leds", DEVPATH=="*/*:flash", RUN+="/bin/chgrp -R torch /sys%p", RUN+="/bin/chmod -R g=u /sys%p"
    ``
3. Create group ``torch``
    >``sudo groupadd torch``
4. Add you into torch group
    >``sudo usermod -a -G torch $USER``
5. Clone repo into extensions folder
    >``git clone https://gitlab.com/NekoCWD/nekotorch.git ~/.local/share/gnome-shell/extensions/nekotorch@nekocwd.gitlab.com``
6. Reboot
7. Enable extension via ``gnome-extensions-app``
