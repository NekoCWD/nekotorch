import GObject from "gi://GObject";
import Adw from "gi://Adw";
import Gtk from "gi://Gtk";
import GLib from "gi://GLib";
import Gio from "gi://Gio";
import GioUnix from "gi://GioUnix";

export const decoder = new TextDecoder("utf-8");
export const encoder = new TextEncoder("utf-8");

export function read_form_fd(fd) {
  var stream = new Gio.DataInputStream({
    base_stream: new GioUnix.InputStream({ fd: fd }),
  });

  var [arr, l] = stream.read_line(null);
  var output = [];
  while (arr != null) {
    output.push(decoder.decode(arr));
    [arr, l] = stream.read_line(null);
  }

  return output;
}

export function spawn(command, path = "./") {
  var [res, pid, stdin, stdout, stderr] = GLib.spawn_async_with_pipes(
    path,
    command,
    null,
    GLib.SpawnFlags.SEARCH_PATH,
    null,
  );
  return [res, read_form_fd(stdout), read_form_fd(stderr)];
}

export function spawn_privileged(command, path = "./") {
  return spawn(["pkexec"].concat(command), path);
}

export function is_in_group(group = "torch") {
  return spawn(["groups"])[1].join(" ").indexOf(group) != -1;
}

export function get_username() {
  return spawn(["whoami"])[1][0];
}

export function get_rule(rule_path) {
  // TODO
}

export function make_command_group(
  command,
  privileged,
  title,
  description = null,
  state = 0,
) {
  const group = new Adw.PreferencesGroup({
    title: title,
    description: description,
  });
  group.add(make_command_row(command, privileged, state));
  return group;
}

export function make_command_row(command, privileged, state = 0) {
  if (privileged) {
    command = ["pkexec"].concat(command);
  }

  const row = new Gtk.Box({
    orientation: Gtk.Orientation.HORIZONTAL,
    css_classes: ["card", "view"],
  });
  const labels = new Gtk.Box({
    orientation: Gtk.Orientation.VERTICAL,
  });

  const tmpl = {
    margin_end: 12,
    margin_start: 12,
    margin_top: 8,
    margin_bottom: 8,
    wrap: true,
    use_markup: true,
    hexpand: true,
    xalign: 0,
    visible: false,
  };

  const cmd = new Gtk.Label(tmpl);
  cmd.visible = true;
  cmd.label =
    '<span foreground="' +
    (privileged ? "red" : "green") +
    '"># </span><b>' +
    command.join(" ") +
    "</b>";
  labels.append(cmd);
  const stderr = new Gtk.Label(tmpl);
  labels.append(stderr);
  stderr.add_css_class("error");
  const stdout = new Gtk.Label(tmpl);
  stdout.add_css_class("success");
  labels.append(stdout);

  const btn = new Gtk.Button({
    icon_name: "media-playback-start-symbolic",
    halign: Gtk.Align.END,
    css_classes: ["flat"],
  });

  btn.connect("clicked", () => {
    stderr.visible = false;
    stdout.visible = false;

    var [res, out, err] = spawn(command);
    set_state(err.length > 0 ? 1 : 2);
    if (err.length > 0) {
      stderr.visible = true;
      stderr.label = "<b>Err: </b>" + err.join("\n");
    }
    if (out.length > 0) {
      stdout.visible = true;
      stdout.label = "<b>Out: </b>" + out.join("\n");
    }
  });

  row.append(labels);
  row.append(btn);

  function set_state(state) {
    switch (state) {
      case 0:
        btn.icon_name = "media-playback-start-symbolic";
        btn.remove_css_class("error");
        btn.remove_css_class("success");
        break;
      case 1:
        btn.icon_name = "process-stop-symbolic";
        btn.add_css_class("error");
        btn.remove_css_class("success");
        break;
      case 2:
        btn.icon_name = "emblem-ok-symbolic";
        btn.remove_css_class("error");
        btn.add_css_class("success");
        break;
    }
  }
  set_state(state);

  return row;
}

export function make_setting_switch(title, subtitle, prop, settings) {
  const new_switch = new Adw.SwitchRow({
    title: title,
    subtitle: subtitle,
  });
  settings.bind(
    prop,
    new_switch,
    "active",
    Gio.SettingsBindFlags.DEFAULT,
  );
  return new_switch;
}

export function setting_to_visibility(widget, prop, settings) {
  settings.bind(
    prop,
    widget,
    "visible",
    Gio.SettingsBindFlags.GET,
  );
}
export function bind_multiple_full(objects, prop, object, prop1, flags = GObject.BindingFlags.DEFAULT, convert_to = null, convert_from = null) {
  for (const obj of objects) {
    if (convert_to == null && convert_from == null) {
      object.bind_property(prop1, obj, prop, flags);
    }
    else {
      object.bind_property_full(prop1, obj, prop, flags, convert_to, convert_from);
    }
  }
}