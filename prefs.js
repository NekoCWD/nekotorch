import Gio from "gi://Gio";
import Adw from "gi://Adw";
import Gtk from "gi://Gtk";

import * as Utils from "./utils.js";

import {
  ExtensionPreferences,
  gettext as _,
} from "resource:///org/gnome/Shell/Extensions/js/extensions/prefs.js";

export default class NekoTorchPreferences extends ExtensionPreferences {
  fillPreferencesWindow(window) {
    const settings = this.getSettings();
    // Setup
    {
      const setup_page = new Adw.PreferencesPage({
        title: _("Setup"),
        icon_name: "utilities-terminal-symbolic",
        description: _("Make initial setup"),
      });
      //window.add(setup_page);
    }
    // Leds
    {
      const leds_enabled = settings.get_strv("used-leds");
      const leds_page = new Adw.PreferencesPage({
        title: _("LEDs"),
        icon_name: "preferences-system-symbolic",
        description: _("Configure which LEDs will be used by the extension"),
      });
      window.add(leds_page);

      const managed_group = new Adw.PreferencesGroup({
        title: _("Supported"),
        description: _("These LEDs managed by NekoTorch udev rule"),
      });
      const unmanaged_group = new Adw.PreferencesGroup({
        title: _("Unsupported"),
        description: _(
          "These LEDs aren't managed by NekoTorch udev rule. Use it you shure, that you can can control it",
        ),
      });
      leds_page.add(managed_group);
      leds_page.add(unmanaged_group);

      const onLedReconfigured = (led) => {
        if (led.get_active()) {
          const pos = leds_enabled.indexOf(led.get_title());
          if (pos == -1) leds_enabled.push(led.get_title());
        } else {
          const pos = leds_enabled.indexOf(led.get_title());
          if (pos != -1) leds_enabled.splice(pos, 1);
        }
        settings.set_strv("used-leds", leds_enabled);
      };

      const leds_not_found = settings.get_strv("used-leds");

      const leds_directory = Gio.File.new_for_path("/sys/class/leds");
      const iter = leds_directory.enumerate_children(
        "standard::*",
        Gio.FileQueryInfoFlags.NOFOLLOW_SYMLINKS,
        null,
      );
      for (const fileInfo of iter) {
        const filename = fileInfo.get_name();
        const file_index = leds_not_found.indexOf(filename);
        if (file_index != -1) {
          leds_not_found.splice(file_index, 1);
        }
        const row = new Adw.SwitchRow({
          title: _(filename),
          subtitle: _("/sys/class/leds/" + filename),
        });
        row.set_active(file_index != -1);
        row.connect("notify::active", onLedReconfigured);
        if (filename.indexOf(":flash") != -1) {
          managed_group.add(row);
        } else {
          unmanaged_group.add(row);
        }
      }

      const not_found_group = new Adw.PreferencesGroup({
        title: _("Not found"),
        description: _(
          "These LEDs wasn't found in your system. You can leave it as is, but it can produce bugs",
        ),
      });

      for (const filename of leds_not_found) {
        const row = new Adw.SwitchRow({
          title: _(filename),
          subtitle: _("/sys/class/leds/" + filename),
        });
        row.set_active(true);
        row.connect("notify::active", onLedReconfigured);
        not_found_group.add(row);
      }
      if (leds_not_found.length != 0) {
        leds_page.add(not_found_group);
      }
    }
    // Behavior
    {
      const behavior_page = new Adw.PreferencesPage({
        title: _("Behavior"),
        icon_name: "application-x-addon-symbolic",
      });
      window.add(behavior_page);
      const look_group = new Adw.PreferencesGroup({
        title: _("Look"),
      });
      behavior_page.add(look_group);
      const tiles_group = new Adw.PreferencesGroup({
        title: _("Tiles"),
        description: _("Quick settings tiles - tiles like network, bluetooth, etc")
      });
      behavior_page.add(tiles_group);
      const actions_group = new Adw.PreferencesGroup({
        title: _("Actions"),
        description: _("Quick settings actions - actions in upper row. Eg: open settigns, lock, etc")
      });
      behavior_page.add(actions_group);
      const behavior_group = new Adw.PreferencesGroup({
        title: _("Behavior"),
      });
      behavior_page.add(behavior_group);

      look_group.add(
        Utils.make_setting_switch(
          "Show torch icon",
          "Show torch icon in status panel while torch is on",
          "show-indicator",
          settings
        )
      );
      tiles_group.add(
        Utils.make_setting_switch(
          "Button tile",
          "Add button tile to quick setings",
          "add-quick-button",
          settings
        )
      );
      tiles_group.add(
        Utils.make_setting_switch(
          "Slider tile",
          "Add slider tile to quick setings",
          "add-quick-slider",
          settings
        )
      );
      actions_group.add(
        Utils.make_setting_switch(
          "Action button",
          "Add button to quick actions",
          "add-action-button",
          settings
        )
      );

      const delay_between_writes = new Adw.SpinRow({
        title: _("Delay between writes"),
        subtitle: _("May help if your led doesn't like fast changes. Time in ms"),
        digits: 0,
        adjustment: new Gtk.Adjustment({
          value: 0,
          lower: 0,
          upper: 10000,
          step_increment: 1,
          page_increment: 1,
          page_size: 1,
        }),
      });
      settings.bind(
        "brightness-set-delay",
        delay_between_writes,
        "value",
        Gio.SettingsBindFlags.DEFAULT,
      );
      behavior_group.add(delay_between_writes);
    }
  }
}
