import GObject from "gi://GObject";
import GLib from "gi://GLib";

export default GObject.registerClass(
    {
        Properties: {
            logs: GObject.ParamSpec.boxed(
                "logs",
                "logs",
                "Logs",
                GObject.ParamFlags.READABLE,
                GLib.strv_get_type(),
            ),
        },
    },
    class Logger extends GObject.Object {
        constructor(tag) {
            super()
            this.tag = tag;
        }
        // Debug messages. Won't be saved
        dbg = (...args) => Logger.loggerf.apply(null, [false, 0, this.tag].concat(args));
        // Info messages. Won't be saved
        info = (...args) => Logger.loggerf.apply(null, [false, 1, this.tag].concat(args));
        // Warnings. Will be saved
        warn = (...args) => Logger.loggerf.apply(null, [true, 2, this.tag].concat(args));
        // Errors. Will be saved
        err = (...args) => Logger.loggerf.apply(null, [true, 3, this.tag].concat(args));

        static logs = [];

        static loggerf = (need_save, level, logtag, ...args) => {
            let tag = "";
            switch (level) {
                case 0:
                    tag = "DEBUG";
                    break;
                case 1:
                    tag = "INFO";
                    break;
                case 2:
                    tag = "WARN";
                    break;
                case 3:
                    tag = "ERROR";
                    break;
                default:
                    tag = "MEOW";
            }
            let message = '[' + logtag + '] (' + tag + ') => ';
            console.log.apply(console, [message].concat(args));
            if (need_save) Logger.logs.push({ level: level, body: args.join(' '), tag: logtag });
        }
    }
);