/* extension.js
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */
import GObject from "gi://GObject";
import GLib from "gi://GLib";
import Gio from "gi://Gio";
import St from "gi://St";


import * as Main from "resource:///org/gnome/shell/ui/main.js";

import * as Utils from "./utils.js";
import Logger from "./logger.js";

import {
  Extension,
  gettext as _,
} from "resource:///org/gnome/shell/extensions/extension.js";
import {
  QuickToggle,
  QuickSlider,
  SystemIndicator,
  QuickSettingsItem
} from "resource:///org/gnome/shell/ui/quickSettings.js";

var icon_off = null;
var icon_on = null;
const log = new Logger("Extension");


const TorchController = GObject.registerClass(
  {
    Properties: {
      leds: GObject.ParamSpec.boxed(
        "leds",
        "leds",
        "A property holding array of led names",
        GObject.ParamFlags.READWRITE,
        GLib.strv_get_type(),
      ),
      brightness: GObject.ParamSpec.int(
        "brightness",
        "brightness",
        "A property holding brightness of torch",
        GObject.ParamFlags.READWRITE,
        0,
        100,
        100,
      ),
      delay_between_writes: GObject.ParamSpec.int(
        "delay_between_writes",
        "delay_between_writes",
        "A property holding delay between writes brightness of torch",
        GObject.ParamFlags.READWRITE,
        0,
        10000,
        0,
      ),
      enabled: GObject.ParamSpec.boolean(
        "enabled",
        "enabled",
        "A property holding torch state",
        GObject.ParamFlags.READWRITE,
        false,
      ),
    },
  },
  class TorchController extends GObject.Object {
    constructor() {
      super();
      this._can_set = true;
    }
    _set_state() {
      for (const led_name of this.leds) {
        try {
          const led_dir = Gio.File.new_for_path("/sys/class/leds/" + led_name);
          const led_file = led_dir.get_child("brightness");
          const max_brightness = Utils.decoder.decode(
            led_dir.get_child("max_brightness").load_contents(null)[1],
          );
          var new_brightness = Math.round(
            (max_brightness / 100.0) * this.brightness,
          );
          if (new_brightness == 0 && this.brightness != 0) new_brightness = 1;
          new_brightness *= this.enabled ? 1 : 0;
          led_file.replace_contents(
            Utils.encoder.encode(new_brightness),
            null,
            false,
            Gio.FileCreateFlags.NONE,
            null,
          );
        } catch (error) {
          log.error(error);
        }
      }
    }
    update_state() {
      // Some devices works vary laggy when brightness changes fast
      const wait = (ms) => new Promise((resolve) => setTimeout(resolve, ms));
      if (!this._can_set) {
        return;
      }
      this.c = false;
      wait(this.delay_between_writes).then(() => {
        this._can_set = true;
        this._set_state();
      });
    }
    get leds() {
      if (this._leds === undefined) this._leds = Array();
      return this._leds;
    }
    set leds(val) {
      if (this.leds == val) return;
      this._leds = val;
      this.notify("leds");
      this.update_state();
    }
    get brightness() {
      if (this._brightness === undefined) this._brightness = 100;
      return this._brightness;
    }
    set brightness(val) {
      if (this.brightness == val) return;
      this._brightness = val;
      this.notify("brightness");
      this.update_state();
    }
    get enabled() {
      if (this._enabled === undefined) this._enabled = false;
      if (this.brightness == 0) this.brightness = 1;
      return this._enabled;
    }
    set enabled(val) {
      if (this.enabled == val) return;
      this._enabled = val;
      this.notify("enabled");
      this.update_state();
    }
  },
);

const TorchSlider = GObject.registerClass(
  class TorchSlider extends QuickSlider {
    constructor() {
      super({
        gicon: icon_off,
        iconLabel: _("Torch"),
        style_class: "button quick-toggle quick-slider",
        toggleMode: true,
      });
      this.reactive = true;
      this.can_focus = true;
      this.slider.accessible_name = _("Torch slider");
      this.slider.add_style_class_name("NekoTorchToggle");
      this._iconButton.add_style_class_name("NekoTorchToggle");
    }
  },
);

const TorchIndicator = GObject.registerClass(
  class TorchIndicator extends SystemIndicator {
    constructor(settings) {
      super();

      this._indicator = this._addIndicator();
      this._indicator.gicon = icon_on;

      this.torchController = new TorchController();

      this.slider = new TorchSlider();

      this.toggleButton = new QuickToggle({
        title: _("Torch"),
        gicon: icon_off,
        toggleMode: true,
      });

      this.actionButton = new QuickSettingsItem({
        toggle_mode: true,
        can_focus: true,
        accessible_name: _('Torch'),
        style_class: 'icon-button',
        child: new St.Icon(),
      });


      // (GSettings use-button) => (ToggleButton visible)
      Utils.setting_to_visibility(this.toggleButton, "add-quick-button", settings);
      Utils.setting_to_visibility(this.slider, "add-quick-slider", settings);
      Utils.setting_to_visibility(this.actionButton, "add-action-button", settings);


      // (GSettings used-leds) => (Torch leds)
      settings.bind(
        "used-leds",
        this.torchController,
        "leds",
        Gio.SettingsBindFlags.GET,
      );
      // (GSettings brightness) <=> (Torch brightness)
      settings.bind(
        "brightness",
        this.torchController,
        "brightness",
        Gio.SettingsBindFlags.DEFAULT,
      );
      // (GSettings brightness-set-delay) => (Torch delay-between-writes)
      settings.bind(
        "brightness-set-delay",
        this.torchController,
        "delay-between-writes",
        Gio.SettingsBindFlags.GET,
      );
      // (Torch state)*(GSettings show-indicator) => (Indicator visible)
      this.torchController.bind_property_full(
        "enabled",
        this._indicator,
        "visible",
        GObject.BindingFlags.SYNC_CREATE,
        (bind, val) => {
          return [true, val && settings.get_boolean("show-indicator")];
        },
        null,
      );

      Utils.bind_multiple_full(
        [this.slider, this.toggleButton, this.actionButton.child],
        "gicon",
        this.torchController,
        "enabled",
        GObject.BindingFlags.SYNC_CREATE,
        (_, val) => { return [true, val ? icon_on : icon_off]; }
      );

      Utils.bind_multiple_full(
        [this.slider, this.toggleButton, this.actionButton],
        "checked",
        this.torchController,
        "enabled",
        GObject.BindingFlags.SYNC_CREATE | GObject.BindingFlags.BIDIRECTIONAL
      );


      // (Slider value) => (Torch state)
      this.slider.slider.bind_property_full(
        "value",
        this.torchController,
        "enabled",
        GObject.BindingFlags.DEFAULT,
        (bind, val) => {
          return [true, val > 0];
        },
        null,
      );


      var slider_binding = null;
      this.slider.connect("notify::checked", () => {
        const state = this.slider.checked;
        if (state) {
          // (Torch brightness) <=> (Slider value)
          slider_binding = this.torchController.bind_property_full(
            "brightness",
            this.slider.slider,
            "value",
            GObject.BindingFlags.SYNC_CREATE |
            GObject.BindingFlags.BIDIRECTIONAL,
            (bind, val) => {
              return [true, val / 100.0];
            },
            (bind, val) => {
              return [true, Math.round(val * 100)];
            },
          );
        } else {
          // UNBIND (Torch brightness) <=/=> (Slider value)
          slider_binding.unbind();
          this.slider.slider.value = 0;
        }
        this.torchController.enabled = state;
      });

      this.quickSettingsItems.push(this.slider);
      this.quickSettingsItems.push(this.toggleButton);
      //Main.panel.statusArea.quickSettings._system._systemItem.child.add_child(this.actionButton);
    }
  },
);

export default class NekoTorchExtension extends Extension {
  enable() {
    icon_on = Gio.icon_new_for_string(
      GLib.build_filenamev([this.path, "icons", "flashlight-symbolic.svg"]),
    );
    icon_off = Gio.icon_new_for_string(
      GLib.build_filenamev([this.path, "icons", "flashlight-off-symbolic.svg"]),
    );
    GLib.idle_add(GLib.PRIORITY_DEFAULT, () => {
      if (Main.panel.statusArea.quickSettings._system == undefined)
        return GLib.SOURCE_CONTINUE;

      this._indicator = new TorchIndicator(this.getSettings());
      Main.panel.statusArea.quickSettings.addExternalIndicator(this._indicator);

      return GLib.SOURCE_REMOVE;
    });
  }

  disable() {
    if (this._indicator != null) {
      this._indicator.actionButton.destroy();
      this._indicator.quickSettingsItems.forEach((item) => item.destroy());
      this._indicator.destroy();
    }
  }
}
